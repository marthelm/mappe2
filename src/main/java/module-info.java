module edu.ntnu.idatt2001.marthelm {
    requires javafx.controls;
    requires javafx.fxml;

    /**
     * Modules exported and opened to javafx modules
     */
    exports edu.ntnu.idatt2001.marthelm;
    opens edu.ntnu.idatt2001.marthelm to javafx.controls, javafx.fxml;
}