package edu.ntnu.idatt2001.marthelm;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.util.Iterator;

/**
 * Class that contains a registry of all patients in lists
 *
 */
public class PatientRegister {
    /**
     * the patient list
     */
    private ObservableList<Patient> patients;

    /**
     * constructor
     */
    public PatientRegister(){
        this.patients = FXCollections.observableArrayList();
    }

    /**
     * get method
     * @return the list patients
     */
    public ObservableList<Patient> getPatients() {
        return patients;
    }

    /**
     * add Method
     *
     * @param patient the Patient
     */
    public void addPatient(Patient patient){
            patients.add(patient);
    }

    /**
     * Adds patients to list
     * @param list the list
     */
    public void addAllPatientsToList(ObservableList<Patient> list){
        if(list.isEmpty()){
            return;
        }else patients.addAll(list);
    }

    /**
     * Method to edit a patient
     * @param patient   the Patient
     * @param newFirstName  the new first name
     * @param newLastName   the new last name
     * @param generalP  the new General Practitioner
     * @param newSocialSecurityNumb the new sosial security number
     */
    public void editPatient(Patient patient, String newFirstName, String newLastName, String generalP, String newSocialSecurityNumb){
        Iterator<Patient> iter = patients.iterator();
        while(iter.hasNext()){
            Patient p = iter.next();
            if(p.getFirstName().equalsIgnoreCase(patient.getFirstName())){
                p.setFirstName(newFirstName);
                p.setLastName(newLastName);
                p.setGeneralPractitioner(generalP);
                p.setSocialSecurityNumber(newSocialSecurityNumb);
            }
        }
    }

    /**
     * method to delete a patient
     * @param patient the patient
     */
    public void deletePatient(Patient patient) {
            patients.remove(patient);
    }
}
