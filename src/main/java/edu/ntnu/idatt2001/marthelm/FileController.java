package edu.ntnu.idatt2001.marthelm;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

public class FileController {

    private App app;

    PatientMainController mainController;
    PatientRegister register;
    CSVHandler<Patient> csvHandler;


    public FileController(){

    }

    private boolean csvValidation(File file){
        String[] fileNameFromList;
        String fileName = (file != null) ? file.getName() : "";
        try{
            fileNameFromList = fileName.split("[.]");
            String fileType = (fileNameFromList.length > 0) ? fileNameFromList[1] : "";
            if(fileType.isBlank() || !fileType.equals("csv")){
                return false;
            }
        }catch (ArrayIndexOutOfBoundsException | NullPointerException e){
            e.printStackTrace();
        }
        return true;
    }



    public void importCSV(Window window )throws IOException {
        ObservableList<Patient> observableList;
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Files", "*.*"),
                new FileChooser.ExtensionFilter("txt files", "*.txt"),
                new FileChooser.ExtensionFilter("Image files", "*.img", "*.png", "*.jpeg", "*.gif"),
                new FileChooser.ExtensionFilter("CSV", "*.csv"));
        fileChooser.setTitle("Open resources file");
        fileChooser.setInitialDirectory(new File("src/main/resources/DataStorage"));
        File selectedFile = fileChooser.showOpenDialog(window);
        boolean run = true;
        while (run) {
            if (csvValidation(selectedFile)) {
                try {
                    observableList = FXCollections.observableArrayList(csvHandler.readCSV(selectedFile));
                    register.getPatients().clear();
                    register.addAllPatientsToList(observableList);
                    mainController.tableView.setItems(register.getPatients());
                    mainController.tableView.refresh();
                    run = false;
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            } else {
                Alert a = new Alert(Alert.AlertType.CONFIRMATION);
                a.setTitle("Invalid file");
                a.setHeaderText("File type not valid");
                a.setContentText("Press show files to open file chooser or cancel to abort");
                ButtonType buttonTypeShow = new ButtonType("Show Files");
                ButtonType buttonTypeCancel = new ButtonType("Cancel");
                a.getButtonTypes().setAll(buttonTypeShow, buttonTypeCancel);
                Optional<ButtonType> result = a.showAndWait();
                if (result.get().equals(buttonTypeShow)) {
                    selectedFile = fileChooser.showOpenDialog(window);
                    if (selectedFile == null) {
                        run = false;
                    }
                }
            }
        }
    }


    public void exportCSV(Window window) throws IOException{
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource csv");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV", "*.csv"));
        fileChooser.setInitialDirectory(new File("src/main/resources/DataStorage"));
        File selectedFile = fileChooser.showSaveDialog(window);
        if(selectedFile != null){
            try{
                csvHandler.writeCSV(selectedFile, register.getPatients());
            }catch (IOException ioe){
                Factory.createAlert("ERROR ON EXPORT", "Error occurred during load", ioe.getMessage());
                ioe.printStackTrace();
            }
        }else {
            Factory.createAlert("WRONG or NO FILE", "Wrong file Alert", "something wrong with file you chose");
        }
    }

}
