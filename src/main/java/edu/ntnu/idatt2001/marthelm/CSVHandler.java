package edu.ntnu.idatt2001.marthelm;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class CSVHandler<T extends Patient> {

    FileWriter fileWriter;
    PatientRegister patientRegister;
    private static final String DELIMITER = ";";
    private static final String NEW_LINE = "\n";

    public CSVHandler(){
        patientRegister = new PatientRegister();
    }


    public void writeCSV(File file, ObservableList<T> patientList) throws IOException {
        try{
            fileWriter = new FileWriter(file);
            for (Patient p : patientList){
                fileWriter.append(String.valueOf(p.getFirstName()));
                fileWriter.append(DELIMITER);
                fileWriter.append(String.valueOf(p.getLastName()));
                fileWriter.append(DELIMITER);
                fileWriter.append(String.valueOf(p.getGeneralPractitioner()));
                fileWriter.append(DELIMITER);
                fileWriter.append(String.valueOf(p.getSocialSecurityNumber()));
                fileWriter.append(NEW_LINE);
            }
        }catch (IOException ioe){
            ioe.printStackTrace();
        }finally {
            fileWriter.flush();
            fileWriter.close();
        }
    }


    public List<Patient> readCSV(File file) throws IOException{
        BufferedReader bufferedReader = null;
        List<Patient> patients = new ArrayList<>();
        try{
            String line = "";

            bufferedReader = new BufferedReader(new FileReader(file));
            while((line = bufferedReader.readLine()) != null){
                String[] rowData = line.split(DELIMITER);
                if (rowData.length > 0){
                    try{
                        Patient patient = new Patient(rowData[0], rowData[1], rowData[2], rowData[3] );
                        patients.add(patient);
                    }catch (IndexOutOfBoundsException ioob){
                        ioob.printStackTrace();
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        } finally {
           try{
               bufferedReader.close();
           }catch (NullPointerException  | IOException exception){
               exception.printStackTrace();
           }

        }
        return patients;

    }
}
