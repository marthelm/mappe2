package edu.ntnu.idatt2001.marthelm;

import java.util.Objects;

/**
 * The Patient class
 */
public class Patient {
    /**
     * The object variables
     */
    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;

    /**
     * Constructor with all parameters including diagnosis
     * @param firstName the first Name
     * @param lastName  the last Name
     * @param diagnosis the diagnosis
     * @param generalPractitioner   the General Practitioner
     * @param socialSecurityNumber  the Social Security Number
     */
    public Patient (String firstName, String lastName, String diagnosis, String generalPractitioner, String socialSecurityNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * constructor with all parameters for each Column in tableView
     * @param firstName The first Name
     * @param lastName  The last Name
     * @param generalPractitioner   The General Practitioner
     * @param socialSecurityNumber  The social Security Number
     */
    public Patient (String firstName, String lastName, String generalPractitioner, String socialSecurityNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.generalPractitioner = generalPractitioner;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Get method
     * @return  social Security Number
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * Set Method
     * @param socialSecurityNumber the social security Number
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * get Method
     * @return first Name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * set Method
     * @param firstName the First Name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * get Method
     * @return the Last Name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set method
     * @param lastName  the last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * get Method
     * @return diagnosis
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    /**
     * set Method
     * @param diagnosis diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * get method
     * @return the general practitioner
     */
    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    /**
     * set Method
     * @param generalPractitioner the general practitioner
     */
    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * return method for a toString that complies with the delimiter for the csv
     * @return all values separated by ";"
     */
    @Override
    public String toString(){
        return firstName + ";" +
                lastName + ";" +
                diagnosis + ";" +
                generalPractitioner + ";" +
                socialSecurityNumber + "\n";
    }

    /**
     * Equals method
     * @param obj the patient object
     * @return boolean value if both social security numbers match or not
     */
    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;
        }
        if(!(obj instanceof Patient)){
            return false;
        }
        Patient otherPatient = (Patient) obj;
        return socialSecurityNumber.equalsIgnoreCase(otherPatient.getSocialSecurityNumber());
    }

    /**
     * A hash method for social security number
     * @return hash for social security number
     */
    @Override
    public int hashCode() {
        return Objects.hash(socialSecurityNumber);
    }
}
