package edu.ntnu.idatt2001.marthelm;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;

/**
 * Controller for the dialogue pane when adding or editing patient
 */
public class AddPatientDialogController {
    /**
     * firstname
     */
    @FXML
    private TextField firstName;
    /**
     * lastname
     */
    @FXML
    private TextField lastName;
    /**
     * general practitioner
     */
    @FXML
    private TextField generalPractitioner;
    /**
     * sosial security number
     */
    @FXML
    private TextField socialSecurityNumber;

    /**
     * Method that processes information from the new inputs into each of the fxml connected fields
     * @return the newly edited patient
     */
    public Patient processPatient(){
        String fn = firstName.getText().trim();
        String ln = lastName.getText().trim();
        String genPrac = generalPractitioner.getText().trim();
        String ssn = socialSecurityNumber.getText().trim();

        return new Patient(fn, ln, genPrac, ssn);
    }
}
