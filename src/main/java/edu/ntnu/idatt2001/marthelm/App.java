package edu.ntnu.idatt2001.marthelm;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.io.IOException;

/**
 * JavaFX App
 */
public class App extends Application {
    Stage stage;
    /**
     * The scene
     */
    private static Scene scene;

    /**
     * Start method from Application
     * @param stage the Stage
     * @throws IOException an IOException
     */
    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("PatientMain"), 640, 480);
        stage.setScene(scene);
        stage.setTitle("HOSPITAL");
        stage.show();
    }

    /**
     * Sets the root
     * @param fxml the fxml
     * @throws IOException  an IOException
     */
    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    /**
     * Load method
     * @param fxml  the fxml
     * @return loaded fxml
     * @throws IOException an IOException
     */
    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    /**
     * The main method
     * @param args  every argument in an array
     */
    public static void main(String[] args) {
        launch();
    }

    /**
     * Get method for the stage
     * @return the stage
     */
    public Stage getStage(){
        return stage;
    }

    /**
     * Init method from Application
     * @throws Exception an Exception
     */
    @Override
    public void init() throws Exception {
        super.init();
    }
}