package edu.ntnu.idatt2001.marthelm;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

import java.util.Optional;

/**
 * Class that acts at an intermediary for methods to create certain GUI elements
 */
public class Factory {

    public static Node createNode(String nodename){
        if(nodename.isBlank()){
            return null;
        }else if(nodename.equalsIgnoreCase("tableView")){
            return new TableView<>();
        }else if(nodename.equalsIgnoreCase("button")){
            return new Button();
        }else if(nodename.equalsIgnoreCase("vbox")){
            return new VBox();
        }else if(nodename.equalsIgnoreCase("toolbar")){
            return new ToolBar();
        }else if(nodename.equalsIgnoreCase("menubar")){
            return new MenuBar();
        }else if(nodename.equalsIgnoreCase("textfield")){
            return new TextField();
        }else if(nodename.equalsIgnoreCase("gridpane")){
            return new GridPane();
        }
        return null;
    }

    /**
     * Method that creates an Alert of type Warning
     * @param title the title
     * @param header    the header
     * @param content   the content
     */
    public static void createAlert(String title, String header, String content){
        Alert a = new Alert(Alert.AlertType.WARNING);
        a.setTitle(title);
        a.setHeaderText(header);
        a.setContentText(content);
        a.show();
    }

    /**
     * Method that creates a confirmation Alert
     * @param header    the header
     * @param content   the content
     * @return boolean value if ok or cancel
     */
    public static boolean createConfirmationDialog(String header, String content){
        Alert a = new Alert(Alert.AlertType.CONFIRMATION);
        a.setTitle("Confirmation dialog");
        a.setHeaderText(header);
        a.setContentText(content);
        Optional<ButtonType> result = a.showAndWait();
        if(result.get() == ButtonType.OK){
            return true;
        }
        return false;
    }
}
