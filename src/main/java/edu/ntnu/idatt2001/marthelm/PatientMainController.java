package edu.ntnu.idatt2001.marthelm;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;

public class PatientMainController {


    @FXML
    private VBox mainVbox;
    @FXML
    public TableView<Patient> tableView;
    @FXML
    private TableColumn<Patient, String> firstNameColumn;
    @FXML
    private TableColumn<Patient, String> lastNameColumn;
    @FXML
    private TableColumn<Patient, String> genPracColumn;
    @FXML
    private TableColumn<Patient, String> socialSecurityNumbColumn;

    PatientRegister patientRegister = new PatientRegister();
    static CSVHandler csvHandler = new CSVHandler();
    FileController fileController = new FileController();
    App app;


    public void initialize(){
        setCellProperty();
        tableView.setItems(patientRegister.getPatients());
    }

    private void setCellProperty(){
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        genPracColumn.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
        socialSecurityNumbColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
    }
    @FXML
    public void handleAboutMenuItem(){
        Alert a = new Alert(Alert.AlertType.INFORMATION);
        a.setTitle("About");
        a.setHeaderText("Patient Register \n1-0.SNAPSHOT");
        a.setContentText("Made by a gamer \nThe Factory stuff is annoying...\n I HATE CSV SOO GODDAMNED MUCH\n git commit -m destroy csv\n csv not working.....");
        a.showAndWait();
    }

    @FXML
    public void addNewPatient(){
        Patient p = addAndEditMethod("Add", "Used to add a new Patient");
        try{
            if(patientRegister.getPatients().contains(p)){
                throw new IllegalArgumentException("Already in list");
            }else{
                assert p != null;
                if(p.getFirstName().isEmpty()){
                    Factory.createAlert("Invalid Input", "No First Name", "Please input a valid First Name");
                }else if(p.getLastName().isEmpty()){
                    Factory.createAlert("Invalid Input", "No Last Name", "Please input a valid Last Name");
                }else if(p.getGeneralPractitioner().isEmpty()){
                    Factory.createAlert("Invalid Input", "No General Practitioner", "Please input a valid General Practitioner");
                }else if((p.getSocialSecurityNumber().length() != 11 && !p.getSocialSecurityNumber().matches("[0-9]+"))){
                    Factory.createAlert("Invalid Input", "Invalid Number", "Please input Social Security Number with 11 digits");
                } else {
                    patientRegister.addPatient(p);
                    tableView.setItems(patientRegister.getPatients());
                }
            }
        }catch (NullPointerException npe){
            npe.printStackTrace();
        }
        tableView.refresh();
    }

    @FXML
    public void editPatient(){
        try{
            Patient patient = addAndEditMethod("Edit", "Use this window to edit selected patient");
            assert patient != null;
            if(patient.getFirstName().isEmpty()){
                Factory.createAlert("Invalid Input", "No First Name", "Please input a valid First Name");
            }else if(patient.getLastName().isEmpty()){
                Factory.createAlert("Invalid Input", "No Last Name", "Please input a valid Last Name");
            }else if(patient.getGeneralPractitioner().isEmpty()){
                Factory.createAlert("Invalid Input", "No General Practitioner", "Please input a valid General Practitioner");
            }else if((patient.getSocialSecurityNumber().length() != 11 && !patient.getSocialSecurityNumber().matches("[0-9]+"))){
                Factory.createAlert("Invalid Input", "Invalid Number", "Please input Social Security Number with 11 digits");
            } else {
                patientRegister.editPatient(tableView.getSelectionModel().getSelectedItem(), patient.getFirstName(), patient.getLastName(), patient.getDiagnosis(), patient.getSocialSecurityNumber());
            }
        }catch (NullPointerException npe ){
            npe.printStackTrace();
        }
        tableView.refresh();
    }

    @FXML
    public void removeButton(){
        Patient p = tableView.getSelectionModel().getSelectedItem();
        if(p != null){
            removePatient(p);
        }
    }

    private void removePatient(Patient patient){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Delete");
        alert.setHeaderText("Are you sure?");
        alert.setContentText("Press OK to delete " +patient.getFirstName()+ ", Cancel to abort");
        Optional<ButtonType> result = alert.showAndWait();
        if(result.isPresent() && result.get() == ButtonType.OK){
            patientRegister.deletePatient(patient);
            tableView.refresh();
        }
    }


    private Patient addAndEditMethod(String handle, String message){
        Dialog<ButtonType> editDialog = new Dialog<>();
        editDialog.initOwner(mainVbox.getScene().getWindow());
        editDialog.setTitle(handle + " Patient");
        editDialog.setHeaderText(message);
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("AddPatientDialog.fxml"));
        try{
            editDialog.getDialogPane().setContent(fxmlLoader.load());
        }catch (IOException ioe){
            ioe.printStackTrace();
        }

        editDialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        editDialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
        Optional<ButtonType> result = editDialog.showAndWait();

        if(result.isPresent() && result.get() == ButtonType.OK){
            AddPatientDialogController apdc = fxmlLoader.getController();
            return apdc.processPatient();
        }else return null;
    }

    @FXML
    public void importFromCSVMethod() throws IOException {
        fileController.importCSV(mainVbox.getScene().getWindow());
    }

    @FXML
    public void exportToCSVMethod() throws IOException {
        fileController.exportCSV(mainVbox.getScene().getWindow());
    }

    @FXML
    public void exitMethod(){
        Platform.exit();
    }


}
